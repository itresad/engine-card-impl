[![pipeline status](https://gitlab.com/tcgone/engine-card-impl/badges/master/pipeline.svg)](https://gitlab.com/tcgone/engine-card-impl/commits/master)

TCG ONE's card implementation repository.

Please see <https://forum.tcgone.net/t/card-implementation-guide/26> for updated instructions.
